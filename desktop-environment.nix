{ config, pkgs, lib, inputs, ... } @values:

with lib; let
  cursor-pkg = inputs.rose-pine-hyprcursor.packages.${pkgs.system}.default;
in {
  options = {
    desktopEnvironment = {
      hyprlandExtraConfig = mkOption {
        type = types.listOf types.str;
        default = "";
      };
      gammastepLocation = mkOption {
        type = types.str;
        default = "41:-77";
      };
      hyprpaperConfig = mkOption {
        type = types.attrs;
        default = {};
      };
      waybarHeight = mkOption {
        type = types.int;
        default = 28;
      };
    };
  };

  config = {
    environment.systemPackages = with pkgs; [
      swaynotificationcenter
      helvum
      wev
      playerctl
      gammastep
      cursor-pkg
      elegant-sddm
      seahorse
      grim
      slurp
      wl-clipboard
      gparted
      kdePackages.polkit-kde-agent-1
    ];

    # Display manager

    services.displayManager.sddm = {
      enable = true;
      wayland.enable = true;
      theme = "Elegant";
    };

    services.gnome.gnome-keyring.enable = true;

    security.pam.services = {
      swaylock.text = ''
          auth include login
        '';
      sddm.text = ''
          auth include login
        '';
    };

    home-manager.users.henry = {
      programs.waybar = import ./program-configs/waybar.nix config.desktopEnvironment.waybarHeight;
      programs.fuzzel = import ./program-configs/fuzzel.nix;
      wayland.windowManager.hyprland = import ./program-configs/hyprland.nix config.desktopEnvironment values;
      services.hyprpaper = {
        enable = true;
        settings = config.desktopEnvironment.hyprpaperConfig;
      };

      programs.swaylock = {
        enable = true;
        settings = {
          color = "#7f1862";
          ignore-empty-password = true;
        };
      };

      home.file.".local/share/icons/rose-pine-hyprcursor" = { source = "${cursor-pkg}/share/icons/rose-pine-hyprcursor"; recursive = true; };
      home.file.".local/share/icons/rose-pine-xcursor" = {
        source = builtins.fetchTarball {
          url = "https://github.com/rose-pine/cursor/releases/download/v1.1.0/BreezeX-RosePine-Linux.tar.xz";
          sha256 = "1dzbrrfl4y8pfva8v3p3aakh94dm4sxp8piq34ghbfd1y407175p";
        };
        recursive = true;
      };
    };

    # Enable CUPS to print documents.

    services.printing.enable = true;

    services.avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
    };

    # Pipewire

    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      # If you want to use JACK applications, uncomment this
      jack.enable = true;

      # use the example session manager (no others are packaged yet so this is enabled by default,
      # no need to redefine it in your config for now)
      #media-session.enable = true;
    };

    # Bluetooth

    services.blueman.enable = true;
    hardware.bluetooth.enable = true;
    hardware.bluetooth.powerOnBoot = true;

    systemd.user.services.mpris-proxy = {
      description = "Mpris proxy";
      after = [ "network.target" "sound.target" ];
      wantedBy = [ "default.target" ];
      serviceConfig.ExecStart = "${pkgs.bluez}/bin/mpris-proxy";
    };

    hardware.bluetooth.settings = {
    	General = {
    		Experimental = true;
    	};
    };
  };
}
