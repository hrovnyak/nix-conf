{ config, pkgs, lib, ... }:

let
  secrets = import ./secrets.nix;
in {
  imports = [
    ./base-desktop.nix
    ./hardware/framework-hardware.nix
  ];

  networking.hostName = "framework";

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  services.fwupd.enable = true;

  environment.systemPackages = with pkgs; [
    ddcutil
    nwg-look
  ];

  # Users

  age.secrets.framework-pass.file = ./secrets/framework-pass.age;

  users.users.henry.hashedPasswordFile = config.age.secrets.framework-pass.path;
  fileSystems."/home".neededForBoot = true;

  # SSH

  home-manager.users.henry.programs.ssh.extraConfig = "
    Host data.cs.purdue.edu
      User hrovnyak
    	PreferredAuthentications publickey
    	IdentityFile ~/.ssh/id_ed25519
  ";
  
  # btrfs backups

  fileSystems."/mnt/btrfs" = {
    device = "/dev/disk/by-partlabel/home";
    fsType = "btrfs";
    options = [
      "noatime"
    ];
  };

  environment.etc = {
    "btrbk/btrbk.conf".text = ''
      timestamp_format long
      snapshot_preserve_min 16h
      snapshot_preserve 48h 7d 3w 4m 1y

    	snapshot_dir btrbk-snapshots

      volume /home
        snapshot_dir /btrbk-snapshots
      	subvolume .
    '';
  };

  # DE config

  home-manager.users.henry.programs.alacritty.settings.font.size = lib.mkForce 20;

  powerManagement.powerDownCommands = ''
    systemctl stop wpa_supplicant-wlp170s0
  '';

  powerManagement.powerUpCommands = ''
    systemctl start wpa_supplicant-wlp170s0
  '';

  desktopEnvironment = {
    waybarHeight = 50;
    hyprlandExtraConfig = [ ''
      exec-once = sudo systemctl restart wpa_supplicant-wlp170s0.service

      env = HYPRCURSOR_SIZE,48
      env = XCURSOR_SIZE,48

      monitor = eDP-1, 2256x1504@60, 0x0, 1
      monitor = , preferred, auto, 1, mirror, eDP-1

      decoration:blur:enabled = false
      decoration:shadow:enabled = false
      misc:vfr = true

      input:sensitivity = 0.2
      input:touchpad:scroll_factor = 0.6

      bind = $mod, p, exec, hyprctl keyword monitor "eDP-1, 2256x1269@60, 0x0, 1"
      bind = $mod Shift, p, exec, hyprctl reload

      bind = , XF86MonBrightnessDown, exec, nu /home/henry/dotfiles/hardware/framework-brightness.nu sub
      bind = , XF86MonBrightnessUp, exec, nu /home/henry/dotfiles/hardware/framework-brightness.nu add

      bindl=,switch:on:Lid Switch, exec, swaylock
    '' ];

    hyprpaperConfig = {
      ipc = "off";
      
      preload = [
        "~/nextcloud/Photos/phone/IMG_20221125_4419267.jpg"
      ];

      wallpaper = [
        "eDP-1, ~/nextcloud/Photos/phone/IMG_20221125_4419267.jpg"
      ];
    };
  };

  # State version

  system.stateVersion = "24.05";
}
