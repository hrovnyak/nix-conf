# Nushell Environment Config File

def create_left_prompt [] {
    let path_segment = if (is-admin) {
        $"(ansi red_bold)($env.PWD)"
    } else {
        $"(ansi green_bold)($env.PWD)"
    }

    $path_segment
}

def create_right_prompt [] {
    let time_segment = ([
        (date now | format date '%m/%d/%Y %r')
    ] | str join)

    $time_segment
}

def typst-dir [path: string] {
    if ($path | path split).0 == "tmp" {
        "/tmp/typst"
    } else {
        $"/home/henry/nextcloud/typst"
    }
}

def sitelen [path: string] {
    let typst_dir = typst-dir $path

    let file_path = $typst_dir | path join $"($path).typ"
    let output_path = $typst_dir | path join $"($path).pdf"

    let dirname = $file_path | path dirname

    if (not ($dirname | path exists)) {
        mkdir $dirname
    }

    if (not ($file_path | path exists)) {
        "#import \"@local/henrys-typst-utils:0.1.0\" : *" | save $file_path
    }

    try {
        typst compile $file_path
    }

    let pdf_id = pueue add -i -p xdg-open $output_path

    hx $file_path -w $typst_dir

    pueue kill $pdf_id
    pueue remove $pdf_id
}

def import-to-ereader [file: string, ereader: path = /dev/sda] {
    let typst_file = (typst-dir $file) | path join $"($file).pdf"

    mut file = $file

    if ($typst_file | path exists) {
        $file = $typst_file
    }

    let tmp = mktemp -d
    sudo mount $ereader $tmp
    sudo cp $file $tmp
    sudo umount $tmp
    rm $tmp
}

def --env rg [] {
    let tempfile = (mktemp -t)
    ranger (["--choosedir=", $tempfile] | str join)
    let path = open $tempfile
    cd $path
    zoxide add $path
    rm $tempfile
}

def o-pali [$to, $from] {
    sudo docker build . -t o-pali
    sudo docker run -it -p $"($from):($to)" o-pali
}

alias sona = ssh data.cs.purdue.edu

def --env jasima-sona [] {
    rm ~/projects/data
    mkdir ~/projects/data
    sshfs hrovnyak@data.cs.purdue.edu:. ~/projects/data -o $"uid=(id -u)"
    zoxide add ~/projects/data
    cd ~/projects/data
}

alias stable-diffusion = /opt/stable-diffusion-ui/start.sh

def fuck [] {
    nu -c (thefuck (history | last 1 | get command.0))
}

$env.TF_ALIAS = "fuck"
$env.PYTHONIOENCODING = "utf-8"

# Use nushell functions to define your right and left prompt
$env.PROMPT_COMMAND = {|| create_left_prompt }
$env.PROMPT_COMMAND_RIGHT = {|| create_right_prompt }

# The prompt indicators are environmental variables that represent
# the state of the prompt
$env.PROMPT_INDICATOR = {|| "〉" }
$env.PROMPT_INDICATOR_VI_INSERT = {|| ": " }
$env.PROMPT_INDICATOR_VI_NORMAL = {|| "〉" }
$env.PROMPT_MULTILINE_INDICATOR = {|| "::: " }

$env.EDITOR = "hx" 
$env.VISUAL = "hx" 
$env.SUDO_EDITOR = "/home/henry/.cargo/bin/hx"
$env.ALCRO_BROWSER_PATH = "/usr/bin/brave" 

# Specifies how environment variables are:
# - converted from a string to a value on Nushell startup (from_string)
# - converted from a value back to a string when running external commands (to_string)
# Note: The conversions happen *after* config.nu is loaded
$env.ENV_CONVERSIONS = {
  "PATH": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
  "Path": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
}

# Directories to search for scripts when calling source or use
#
# By default, <nushell-config-dir>/scripts is added
$env.NU_LIB_DIRS = [
    ($nu.config-path | path dirname | path join 'scripts')
]

# Directories to search for plugin binaries when calling register
#
# By default, <nushell-config-dir>/plugins is added
$env.NU_PLUGIN_DIRS = [
    ($nu.config-path | path dirname | path join 'plugins')
]

# To add entries to PATH (on Windows you might use Path), you can use the following pattern:
# $env.PATH = ($env.PATH | split row (char esep) | append ["/usr/lib/jvm/java-17-openjdk", "/usr/local/texlive/2022/bin/x86_64-linux", "/home/henry/.nvm/versions/node/v20.5.0/bin", "/home/henry/.cargo/bin", "/home/henry/.local/bin/bin", "/home/henry/.local/bin/wabt"])

mkdir ~/.cache/starship
starship init nu | save ~/.cache/starship/init.nu -f

if ("/tmp/.zoxide.nu" | path exists) {
    rm /tmp/.zoxide.nu
}

zoxide init nushell | save -f /tmp/.zoxide.nu
