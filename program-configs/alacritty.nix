{
  enable = true;
  settings = {
    env = {
      TERM = "xterm-256color";
      LANG = "en_US.UTF-8";
      LC_CTYPE = "en_US.UTF8";
    };

    window = {
      opacity = 0.8;
    };

    font = {
      normal = {
        family = "monospace";
        style = "Regular";
      };

      size = 11.0;
    };

    colors = {
      primary = {
        background = "#000000";
        foreground = "#C6C6C4";
      };

      normal = {
        black = "#10100E";
        red = "#C40233";
        green = "#009F6B";
        yellow = "#FFD700";
        blue = "#0087BD";
        magenta = "#9A4EAE";
        cyan = "#20B2AA";
        white = "#C6C6C4";
      };

      bright = {
        black = "#696969";
        red = "#FF2400";
        green = "#03C03C";
        yellow = "#FDFF00";
        blue = "#007FFF";
        magenta = "#FF1493";
        cyan = "#00CCCC";
        white = "#FFFAFA";
      };
    };
  };
}
