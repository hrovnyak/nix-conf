height: {
  enable = true;
  settings = {
    mainBar = {
      layer = "top";
      position = "top";
      height = height;

      modules-left = [ "hyprland/workspaces" ];
      modules-center = [ "custom/clock" ];
      modules-right = [ "tray" "pulseaudio" "battery" ];

      "hyprland/workspaces" = {
        move-to-monitor = false;
      };

      "custom/clock" = {
        format = "{}";
        interval = 5;
        exec = "date \"+%b %-d, %-H:%M\"";
      };

      "pulseaudio" = {
        format = "🔊 {volume}%";
      };

      "battery" = {
        format = "⚡ {capacity}%";
        format-discharging = "🔋 {capacity}%, {time}";
      };

      "tray" = {
        "icon-size" = height * 18 / 28;
        "spacing" = height * 8 / 28;
      };
    };
  };

  style = ''
    * {
      /* `otf-font-awesome` is required to be installed for icons */
      font-family: FontAwesome, Roboto, Helvetica, Arial, sans-serif;
      font-size: 0.23in;
      color: white;
    }

    window#waybar {
      background-color: rgba(43, 48, 59, 0.5);
      border-bottom: 3px solid rgba(100, 114, 125, 0.5);
      transition-property: background-color;
      transition-duration: .5s;

      border-bottom: 1px solid rgba(255, 255, 255, 0.5);
    }
  
    #workspaces button {
      border: none;
      border-radius: 0;
      border-right: 1px solid rgba(255, 255, 255, 0.5);
      transition-duration: 200ms;
      margin-bottom: 1px;
    }

    #workspaces button.visible {
      background-color: rgba(255, 46, 199, 0.2);
    }

    #workspaces button.active {
      background-color: rgba(255, 46, 199, 0.4);
    }

    #workspaces button, #custom-clock, #pulseaudio, #battery {
      padding: 0 0.15in;
    }

    #tray {
      margin-right: 0.15in;
    }

    #custom-clock {
      border: 1px solid rgba(255, 255, 255, 0.5);
      border-top: none;
      border-bottom: none;
      margin-bottom: 1px;
      background-color: rgba(255, 46, 199, 0.2);
    }

    #pulseaudio, #battery {
      border: none;
      border-left: 1px solid rgba(255, 255, 255, 0.5);
      margin-bottom: 1px;
    }

    #pulseaudio {
      background-color: rgba(255, 46, 199, 0.2);
    }

    #battery {
      background-color: rgba(184, 151, 0, 0.2);
    }

    #battery.discharging {
      background-color: rgba(99, 145, 255, 0.2);
    }

    #pulseaudio.muted {
      background-color: rgba(0, 0, 0, 0);
    }

    #pulseaudio.bluetooth {
      background-color: rgba(70, 46, 255, 0.2);
    }
  '';
}
