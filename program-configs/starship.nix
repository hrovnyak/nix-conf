{
  enable = true;
  settings = {
    add_newline = false;
    format = "$username$hostname$directory$git_status$character";
    hostname = {
      ssh_only = false;
      ssh_symbol = "🌐";
    };
  };
}
