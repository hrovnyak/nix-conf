de: { lib, pkgs, ... }:

with lib;
let
  workspaces = [
    "1" "2" "3" "4" "5" "6" "7" "8" "9"
  ];

  workspaceBind = builtins.toString (builtins.map (workspace : ''
    bind = $mod, ${workspace},       workspace, ${workspace}
    bind = $mod Ctrl, ${workspace},  movetoworkspacesilent, ${workspace}
    bind = $mod Shift, ${workspace}, movetoworkspace, ${workspace}

  '') workspaces);
in {
  enable = true;
  
  extraConfig = ''
    debug:disable_logs = false

    input:kb_layout = us
    input:kb_options = compose:ralt

    env = HYPRCURSOR_THEME,rose-pine-hyprcursor
    env = XCURSOR_THEME,rose-pine-xcursor
    cursor:no_hardware_cursors = true
    exec-once = dconf write /org/gnome/desktop/interface/cursor-theme "'rose-pine-xcursor'"


    exec-once = waybar
    exec-once = firefox
    exec-once = gammastep -l ${de.gammastepLocation}

    windowrule = pseudo, fcitx
    exec-once=fcitx5 -d -r
    exec-once=fcitx5-remote -r  


    $mod = SUPER;

    bind = $mod, q, killactive,

    bind = $mod, Return, exec, alacritty
    bind = $mod, d,      exec, fuzzel

    bind = $mod, h, movefocus, l
    bind = $mod, j, movefocus, d
    bind = $mod, k, movefocus, u
    bind = $mod, l, movefocus, r

    bind = $mod Shift, h, movewindow, l
    bind = $mod Shift, j, movewindow, d
    bind = $mod Shift, k, movewindow, u
    bind = $mod Shift, l, movewindow, r
    bindm = $mod, Control_L, movewindow

    bind = $mod Ctrl, period, movecurrentworkspacetomonitor, r
    bind = $mod Ctrl, comma,  movecurrentworkspacetomonitor, l

    bind = $mod, f, fullscreen, 0

    bind = Shift     , Print, exec, grim -g "$(slurp)" /home/henry/nextcloud/Photos/screenshots/$(date "+%b-%-d-%Y_%-H:%M_%Ss:%Nn_screenshot.png")
    bind =           , Print, exec, grim -g "$(slurp)" - | wl-copy
    bind = Shift Ctrl, Print, exec, grim -g "$(slurp -o)" /home/henry/nextcloud/Photos/screenshots/$(date "+%b-%-d-%Y_%-H:%M_%Ss:%Nn_screenshot.png")
    bind =       Ctrl, Print, exec, grim -g "$(slurp -o)" - | wl-copy
    layerrule=noanim,selection

    bind = , XF86AudioMute,        exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
    bind = , XF86AudioRaiseVolume, exec, wpctl set-volume "@DEFAULT_AUDIO_SINK@" 5%+
    bind = , XF86AudioLowerVolume, exec, wpctl set-volume "@DEFAULT_AUDIO_SINK@" 5%-
    bind = , XF86AudioPlay,        exec, playerctl play-pause smplayer

    bind = $mod, 0, submap, system 
    submap = system
    bind = , s,      exec, shutdown now
    bind = , r,      exec, reboot
    bind = , l,      exec, systemctl suspend
    bind = , k,      exit,
    bind = , escape, submap, reset
    submap = reset


    bezier = thingy, 0.25, 1, 0.5, 1
    general:gaps_out = 10
    animation = windows,    1, 2, thingy
    animation = layers,     1, 2, thingy
    animation = workspaces, 1, 2, thingy, fade
    
    general:col.active_border = 0xffffffff
    general:col.inactive_border = 0x80ffffff
    misc:disable_splash_rendering = true
    misc:disable_hyprland_logo = true

    ${builtins.toString(builtins.map (v: "${v}\n") de.hyprlandExtraConfig)}

    ${workspaceBind}
  '';
}
