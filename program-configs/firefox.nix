{ config, pkgs, ... }:

{
  enable = true;

  policies = {
    DNSOverHTTPS = {
      Enabled = true;
      Fallback = false;
    };
  };

  profiles.default = {
    isDefault = true;

    # Manually install...
    # clearurls
    # keepassxc-browser
    # search-by-image
    # sidebery
    # skip-redirect
    # smart-referer
    # snowflake
    # sponsorblock
    # tabliss
    # ublock-origin

    search = {
      default = "Ecosia";
      force = true;
      engines = {
        "Ecosia" = {
          urls = [{
            template = "https://ecosia.org/search";
            params = [
              { name = "q"; value = "{searchTerms}"; }
            ];
          }];
        };
      };
    };    

    userChrome = ''
      /* hides the native tabs */
      #TabsToolbar {
        visibility: collapse;
      }
    '';

    settings = {
      "browser.download.dir" = "/tmp"; # Default download directory
      "browser.download.useDownloadDir" = false; # "Always ask where to save files"

      # Use "Manage Exceptions" under "Cookies and Site Data" under "Privacy & Security" under "about:preferences" to save logins for some sites
      "privacy.sanitize.sanitizeOnShutdown" = true; # "Delete cookies and site data when firefox is closed"
      "privacy.clearOnShutdown.cache" = false;
      "privacy.clearOnShutdown.history" = false;
      "privacy.clearOnShutdown.sessions" = false;
      "privacy.clearOnShutdown_v2.historyFormDataAndDownloads" = false;
      "privacy.clearOnShutdown_v2.cache" = false;

      "signon.rememberSignons" = false; # "Ask to save passwords"
      "toolkit.legacyUserProfileCustomizations.stylesheets" = true; # Enable userChrome
      "browser.toolbars.bookmarks.visibility" = false; # Hide bookmarks
      "browser.startup.page" = 3; # Restore previous session
      "layout.css.prefers-color-scheme.content-override" = 0; # Dark mode
      "extensions.activeThemeID" = "firefox-compact-dark@mozilla.org"; # Dark theme
      "devtools.toolbox.host" = "right"; # Dock to right
      "apz.gtk.kinetic_scroll.enabled" = false; # Disable whack scrolling
      "network.protocol-handler.external.mailto" = false; # Disable obnoxious message
      "extensions.formautofill.addresses.enabled" = false; # Disable asking to save my address
      "extensions.formautofill.creditCards.enabled" = false; # Disable asking to save credit cards
    };
  };
}
