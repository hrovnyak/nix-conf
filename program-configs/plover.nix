# Thank you LogicalOverflow! (https://git.sr.ht/~lgcl/nuv/tree/cf205b8e14492e7a853f0b79eecbb31541b66160/item/hmModules/plover.nix)

{
  config,
  pkgs,
  lib,
  ...
}:

let
  inherit (lib) mkEnableOption mkIf;

  plover =
    let
      plover-base = pkgs.plover.dev;

      pp = pkgs.python3Packages;

      plover-output-dotool = pp.buildPythonPackage {
        name = "plover-output-dotool";
        src = pkgs.fetchFromGitHub {
          owner = "halbGefressen";
          repo = "plover-output-dotool";
          rev = "25e7df1a116672163256ccef85cfd91f7e76b9cf";
          hash = "sha256-Fl4/MmXS3NZqgR1E/vl8iJizSeRyhDLH4bhLy92upqY=";
        };

        buildInputs = [ plover-base ];

        propagatedBuildInputs = [ pkgs.dotool ];
      };

      plugins = [
        plover-output-dotool
      ];
    in
    plover-base.overrideAttrs (
      old: { propagatedBuildInputs = (old.propagatedBuildInputs or [ ]) ++ plugins; }
    );

  ploverDesktopItem = pkgs.makeDesktopItem {
    name = "plover";
    desktopName = "Plover";
    comment = "Open stenography";
    exec = ''/usr/bin/env QT_QPA_PLATFORM="" ${plover}/bin/plover'';
    type = "Application";
    terminal = false;
    categories = [ "Utility" ];
  };
in
{
  environment.systemPackages = [
    plover
    ploverDesktopItem
    pkgs.dotool
  ];
}
