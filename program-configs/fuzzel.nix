{
  enable = true;
  settings = {
    main = {
      horizontal-pad = 20;
      vertical-pad = 15;
    };

    colors = {
      background = "000000cc";
      text = "ffffffcc";
      selection = "ff2ec755";
      selection-text = "ffffffff";
      selection-match = "ff7070ff";
      border = "ffffff80";
    };

    border = {
      width = 1;
      radius = 0;
    };
  };
}
