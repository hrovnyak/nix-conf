{ pkgs ? import <nixpkgs> {} }:

let
  rust_overlay = import (builtins.fetchTarball {
    url = "https://github.com/oxalica/rust-overlay/archive/83284068670d5ae4a43641c4afb150f3446be70d.tar.gz";
    sha256 = "sha256:0z5cym494khqy3pxfwfq89nb2981v8q8wb4kxn04i6qj34gjp8ab";
  });
  pkgs = import <nixpkgs> { overlays = [ rust_overlay ]; };
  rust = pkgs.rust-bin.stable.latest.default.override {
    extensions = [ "rust-src" ];
  };
in
pkgs.mkShell {
  buildInputs = [
    rust
  ] ++ (with pkgs; [
    clang
    libgcc
    gdb
    gnumake
    gef

    pkg-config
    rust-analyzer
    sccache

    python3
    python312Packages.python-lsp-server

    typescript
    nodePackages.typescript-language-server
    nodePackages.prettier

    jdk17
    jdt-language-server

    elan
  ]);

  RUST_BACKTRACE = 1;
  RUSTC_WRAPPER = "sccache";
}
