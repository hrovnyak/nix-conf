{ lib, config, pkgs, age, ... }:

{
  services.nginx.virtualHosts."192.168.0.39".listen = [ { addr = "192.168.0.39"; port = 7000; } ];

  networking.firewall.allowedTCPPorts = [ 7000 ];

  age.secrets.nextcloud = {
    file = ../secrets/nextcloud-pass.age;
    owner = "nextcloud";
    group = "nextcloud";
  };

  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud29;
    hostName = "192.168.0.39";

    database.createLocally = true;

    config = {
      dbtype = "pgsql";
      adminpassFile = config.age.secrets.nextcloud.path;
      adminuser = "admin";
    };
  };
}
