def main [todo: string] {
  let backlight = "/sys/class/backlight/intel_backlight"

  let min = 1000
  let max = ((cat ($backlight | path join max_brightness)) | into int) + $min

  let current = ((cat ($backlight | path join brightness)) | into float) + $min

  mut new_brightness = ($current * if $todo == "add" { 2. } else { 0.5 }) | into int

  $new_brightness = (if $new_brightness > $max {
    $max
  } else if $new_brightness < $min + 1 {
    $min + 1
  } else {
    $new_brightness
  }) - $min

  echo ($new_brightness | into string) | (sudo tee ($backlight | path join brightness))
}
