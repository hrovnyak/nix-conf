let 
  ssh-keys = import ../ssh-keys.nix;
in {
  "nextcloud-pass.age".publicKeys = [ ssh-keys.rpi ];
  "rpi-pass.age".publicKeys = [ ssh-keys.rpi ];
  "desktop-pass.age".publicKeys = [ ssh-keys.desktop ];
  "framework-pass.age".publicKeys = [ ssh-keys.framework ];
  "keepass-key-part.age".publicKeys = [ ssh-keys.desktop ssh-keys.framework ];
  "wireless-environment-file.age".publicKeys = [ ssh-keys.rpi ssh-keys.desktop ssh-keys.framework ];
}
