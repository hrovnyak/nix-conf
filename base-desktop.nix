{ config, pkgs, lib, ... } @values:

{
  imports = [
    ./base.nix
    ./desktop-environment.nix
    # ./program-configs/plover.nix
  ];
  
  environment.systemPackages = with pkgs; [
    spotify
    typst
    pdfpc
    polylux2pdfpc
    tinymist
    alacritty
    sage
    nextcloud-client
    keepassxc
    keymapp
    rnote
    libreoffice-qt
    hunspell
    hunspellDicts.en_US
    zoom-us
    anki
    goldendict-ng
    gImageReader
    obs-studio
    authenticator
    planify
    universal-android-debloater
    android-tools
    typst-lsp
    zsa-udev-rules
    gimp
    yt-dlp
    ungoogled-chromium
    newsflash
    inkscape
    calibre
    spotdl

    loupe
    evince
    nautilus
    baobab
    font-manager
    mpv
    gnome-system-monitor
    gparted
  ];

  fonts.packages = with pkgs; [
    noto-fonts
  ];

  fonts.fontconfig = {
    enable = true;
    defaultFonts = {
      serif = [ "Noto Serif" ];
      sansSerif = [ "Noto Sans" ];
    };
  };

  nixpkgs.config.allowUnfree = true;

  programs.steam.enable = true;

  home-manager.users.henry = { lib, ... }: {
    programs.alacritty = import ./program-configs/alacritty.nix;
    programs.firefox = import ./program-configs/firefox.nix values;

    services.nextcloud-client = {
      enable = true;
      startInBackground = true;
    };

    home.file.".XCompose".source = ./.XCompose;
    home.file.".config/plover/plover.cfg".source = ./program-configs/plover.cfg;
    home.file.".config/fcitx5/conf/classicui.conf".text = ''
     Theme=Nord-Dark
     Font="Noto Sans 10"
    '';

    home.activation = {
      linkPlanify = lib.hm.dag.entryAfter ["writeBoundary"] ''
        if [ ! -d ~/.local/share/io.github.alainm23.planify ]; then
          run ln -s ~/nextcloud/archive/io.github.alainm23.planify ~/.local/share/io.github.alainm23.planify
        fi
      '';

      cloneTypstUtils = lib.hm.dag.entryAfter ["writeBoundary"] ''
        if [ ! -d ~/.local/share/typst/packages/local/henrys-typst-utils/0.1.0 ]; then
          run mkdir -p ~/.local/share/typst/packages/local/henrys-typst-utils
          run cd ~/.local/share/typst/packages/local/henrys-typst-utils
          run ${pkgs.git}/bin/git clone https://gitlab.com/hrovnyak/henrys-typst-utils 0.1.0
          run cd 0.1.0
          run ${pkgs.git}/bin/git remote remove origin
          run ${pkgs.git}/bin/git remote add origin git@gitlab.com:hrovnyak/henrys-typst-utils.git
        fi
      '';
    };
  };

  # Keepass key file

  age.secrets.keepass-key-part = {
    file = ./secrets/keepass-key-part.age;
    owner = "henry";
  };
  environment.etc."keepass-key-part".source = config.age.secrets.keepass-key-part.path;

  # Nebulous

  programs.nix-ld.enable = true;

  programs.appimage = {
    enable = true;
    binfmt = true;
  };

  # Startup tasks

  desktopEnvironment.hyprlandExtraConfig = [ ''
    exec-once = plover -g none
    exec-once = keepassxc
  '' ];

  # IME

  i18n.inputMethod = {
    type.enabled = "fcitx5";
    fcitx5 = {
      waylandFrontend = true;
      addons = with pkgs; [
        fcitx5-gtk             # alternatively, kdePackages.fcitx5-qt
        fcitx5-chinese-addons  # table input method support
        fcitx5-nord            # a color theme
      ];
    };
  };


  # ZSA

  users.groups.plugdev = {};
  users.groups.input = {};
  users.users.henry.extraGroups = [ "plugdev" "dialout" "input" ];

  # https://github.com/zsa/wally/wiki/Linux-install
  # https://git.sr.ht/~geb/dotool/tree/master/item/80-dotool.rule
  # services.udev.extraRules = ''
  #   # Rules for Oryx web flashing and live training
  #   KERNEL=="hidraw*", ATTRS{idVendor}=="16c0", MODE="0664", GROUP="plugdev"
  #   KERNEL=="hidraw*", ATTRS{idVendor}=="3297", MODE="0664", GROUP="plugdev"

  #   # Legacy rules for live training over webusb (Not needed for firmware v21+)
  #   # Rule for all ZSA keyboards
  #   SUBSYSTEM=="usb", ATTR{idVendor}=="3297", GROUP="plugdev"
  #   # Rule for the Moonlander
  #   SUBSYSTEM=="usb", ATTR{idVendor}=="3297", ATTR{idProduct}=="1969", GROUP="plugdev"

  #   # Keymapp / Wally Flashing rules for the Moonlander and Planck EZ
  #   SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", MODE:="0666", SYMLINK+="stm32_dfu"
  # '';
}  
