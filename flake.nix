{
  description = "Config";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    home-manager.url = "github:nix-community/home-manager/release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    agenix.url = "github:ryantm/agenix";

    rose-pine-hyprcursor.url = "github:ndom91/rose-pine-hyprcursor";
  };

  outputs = { self, nixpkgs-unstable, nixpkgs, nixos-hardware, home-manager, agenix, ... } @inputs: let
    defaultModules = system: [
      agenix.nixosModules.default
      home-manager.nixosModules.home-manager
      {
        nixpkgs.overlays = [
          (final: _prev: {
            unstable = import nixpkgs-unstable {
              system = system;
            };
          })
        ];
      }
    ];
  in {
    nixosConfigurations = {
      desktop = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = { inherit inputs; };
        modules = (defaultModules "x86_64-linux") ++ [ ./desktop.nix ];
      };

      framework = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = { inherit inputs; };
        modules = (defaultModules "x86_64-linux") ++ [
          ./framework.nix
          nixos-hardware.nixosModules.framework-13th-gen-intel
        ];
      };

      rpi = nixpkgs.lib.nixosSystem {
        system = "aarch64-linux";
        specialArgs = { inherit inputs; };
        modules = (defaultModules "aarch64-linux") ++ [
          ./rpi.nix
          nixos-hardware.nixosModules.raspberry-pi-4
        ];
      };
    };
  };
}
