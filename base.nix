{ config, pkgs, lib, inputs, ... }:

let 
  ssh-keys = import ./ssh-keys.nix;
in {
  imports = [
    ./networks.nix
  ];

  # tmpfs

  fileSystems = {
    "/tmp" = {
      fsType = "tmpfs";
    };
  };

  # Programs

  environment.systemPackages = with pkgs; [
    git
    zoxide
    difftastic
    ranger
    starship
    btrbk
    (builtins.getAttr pkgs.system inputs.agenix.packages).default
    openssl
    gnupg
    usbutils
    unzip
    sshfs
    nushell
  ];

  # Flakes

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Main user

  users = {
    mutableUsers = false;
    users.henry = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      home = "/home/henry";
    };
  };

  # Agenix

  age.identityPaths = [
    "/home/henry/.ssh/id_ed25519"
  ];

  # Enable passwordless sudo.

  security.sudo.extraRules= [
    {  users = [ "henry" ];
      commands = [
         { command = "ALL" ;
           options= [ "NOPASSWD" ];
        }
      ];
    }
  ];

  # Polkit

  security.polkit.enable = true;

  # Programs

  home-manager.users.henry = {
    home.stateVersion = "23.11";
  
    programs.git = {
      enable = true;
      userName = "Henry Rovnyak";
      userEmail = "h.rovnyak@proton.me";
      difftastic.enable = true;
    };

    programs.helix = {
      enable = true;
      defaultEditor = true;
      languages = builtins.fromTOML (builtins.readFile ./program-configs/helix/languages.toml);
      settings = builtins.fromTOML (builtins.readFile ./program-configs/helix/config.toml);
    };

    programs.nushell = {
      enable = true;
      package = pkgs.unstable.nushell;
      envFile.source = ./program-configs/nushell/env.nu;
      configFile.source = ./program-configs/nushell/config.nu;
    };

    programs.direnv = {
      enable = true;
      enableNushellIntegration = true;
      enableBashIntegration = true;
      nix-direnv.enable = true;
    };

    services.pueue.enable = true;

    programs.starship = import ./program-configs/starship.nix;

    programs.ssh = {
      enable = true;
      extraConfig = "
        Host gitlab.com
        	PreferredAuthentications publickey
        	IdentityFile ~/.ssh/id_ed25519

        Host github.com
        	PreferredAuthentications publickey
        	IdentityFile ~/.ssh/id_ed25519
      ";
    };

    programs.gpg = {
      enable = true;
      settings = {
        pinentry-mode = "loopback";
      };
    };

    home.file.".ssh/id_ed25519.pub".text = ssh-keys."${config.networking.hostName}";
    home.file.".ssh/id_ed25519.gpg".source = secrets/${config.networking.hostName}-id.gpg;

    programs.thefuck = {
      enable = true;
      enableNushellIntegration = false;
    };
  };

  users.users.henry.shell = pkgs.nushell;

  environment.shells = [
    pkgs.nushell
  ];

  home-manager.users.root = {
    home.stateVersion = "23.11";

    programs.helix = {
      enable = true;
      defaultEditor = true;
      languages = builtins.fromTOML (builtins.readFile ./program-configs/helix/languages.toml);
      settings = builtins.fromTOML (builtins.readFile ./program-configs/helix/config.toml);
    };

    programs.nushell = {
      enable = true;
      envFile.source = ./program-configs/nushell/env.nu;
      configFile.source = ./program-configs/nushell/config.nu;
    };

    programs.starship = import ./program-configs/starship.nix;

    programs.thefuck = {
      enable = true;
      enableNushellIntegration = false;
    };
  };

  users.users.root.shell = pkgs.nushell;

  # Shell in /tmp

  systemd.tmpfiles.settings = {
    "shell-in-tmp" = {
      "/tmp/shell.nix" = {
        "C+" = {
          group = "root";
          user = "root";
          mode = "444";
          argument = builtins.toPath ./program-configs/tmp-shell/shell.nix;
        };
      };
      "/tmp/.envrc" = {
        "C+" = {
          group = "root";
          user = "root";
          mode = "444";
          argument = builtins.toPath ./program-configs/tmp-shell/.envrc;
        };
      };
    };
  };

  # Localization

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # btrfs backups; the device specific config file must mount the btrfs partition at /mnt/btrfs

  systemd.timers."btrbk-snapshot" = {
    wantedBy = ["timers.target"];
    timerConfig = {
      OnBootSec = "0m";
      OnUnitActiveSec = "1h";
      Unit = "btrbk-snapshot.service";
    };
  };

  systemd.services."btrbk-snapshot" = {
    script = ''
      exec /run/current-system/sw/bin/btrbk -q run
    '';
    serviceConfig = {
      Type = "oneshot";
      User = "root";
    };
  };

  systemd.services.btrbk-snapshots = {
    enable = true;
    description = "Make the btrbk-snapshots directory if it doesn't exist";
    wantedBy = [ "multi-user.target" ];
    script = ''
      if [ ! -d /btrbk-snapshots ]; then
        mkdir /btrbk-snapshots
      fi
    '';
  };

  # SSH

  services.openssh = {
    settings.PasswordAuthentication = false;
  };

  # Annoying programs (NMRPipe I'm looking at you)

  programs.nix-ld.enable = true;
  services.envfs.enable = true;
}
