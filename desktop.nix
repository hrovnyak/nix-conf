{ config, pkgs, lib, ... }:

let
  secrets = import ./secrets.nix;
in {
  imports = [
    ./base-desktop.nix
    ./hardware/desktop-hardware.nix
  ];

  networking.hostName = "desktop";

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  environment.systemPackages = with pkgs; [
    nvidia-vaapi-driver
    boinc
  ];

  # Nvidia

  boot.kernelParams = [
    "nvidia_drm.modeset=1"
  ];

  hardware.opengl = {
    enable = true;
    driSupport32Bit = true;
  };

  services.xserver.videoDrivers = ["nvidia"];

  hardware.nvidia = {
    package = config.boot.kernelPackages.nvidiaPackages.stable;

    modesetting.enable = true;
    open = false;
    nvidiaSettings = true;
  };

  # Users

  age.secrets.desktop-pass = {
    file = ./secrets/desktop-pass.age;
    owner = "henry";
  };

  users.users.henry.hashedPasswordFile = config.age.secrets.desktop-pass.path;

  # btrfs backups

  fileSystems."/mnt/backup-drive" = {
    device = "/dev/disk/by-label/backup-drive";
    fsType = "btrfs";
    options = [
      "noatime"
      "subvol=@desktop-btrbk-snapshots"
    ];
  };

  environment.etc = {
    "btrbk/btrbk.conf".text = ''
      timestamp_format long
      snapshot_preserve_min 16h
      snapshot_preserve 48h 7d 3w
      target_preserve_min 16h
      target_preserve 48h 7d 3w 4m 1y

    	snapshot_dir btrbk-snapshots

      volume /
        target /mnt/backup-drive
      	subvolume .
    '';
  };


  # DE config

  desktopEnvironment = {
    hyprlandExtraConfig = [ ''
      env = LIBVA_DRIVER_NAMNE,nvidia
      env = XDG_SESSION_TYPE,wayland
      env = GBM_BACKEND,nvidia-drm
      env = __GLX_VENDOR_LIBRARY_NAME,nvidia
      env = NVD_BACKEND,direct

      env = HYPRCURSOR_SIZE,24
      env = XCURSOR_SIZE,24

      monitor = DP-2, 1920x1080@60, 1920x0, 1
      monitor = DP-1, 1920x1080@60, 0x0, 1
      monitor = Unknown-1, disable # why does it think a nonexistant monitor exists?
    '' ];

    hyprpaperConfig = {
      ipc = "off";
      
      preload = [
        "~/nextcloud/Photos/ag-carinae.png"
        "~/nextcloud/Photos/carina-nebula.jpg"
        "~/nextcloud/Photos/carina-starforming.jpg"
      ];

      wallpaper = [
        "DP-2, ~/nextcloud/Photos/carina-nebula.jpg"
        "DP-1, ~/nextcloud/Photos/carina-starforming.jpg"
      ];
    };
  };

  # State version

  system.stateVersion = "23.11";
}
