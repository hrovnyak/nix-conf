{ config, pkgs, ... }:

# MAC changer from here: https://nixos.wiki/wiki/Wpa_supplicant

let
	change-mac = pkgs.writeShellScript "change-mac" ''
		card=$1
		tmp=$(mktemp)
		${pkgs.macchanger}/bin/macchanger "$card" -s | grep -oP "[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[^ ]*" > "$tmp"
		mac1=$(cat "$tmp" | head -n 1)
		mac2=$(cat "$tmp" | tail -n 1)
		if [ "$mac1" = "$mac2" ]; then
			if [ "$(cat /sys/class/net/"$card"/operstate)" = "up" ]; then
				${pkgs.iproute2}/bin/ip link set "$card" down &&
				${pkgs.macchanger}/bin/macchanger -r "$card"
				${pkgs.iproute2}/bin/ip link set "$card" up
			else
				${pkgs.macchanger}/bin/macchanger -r "$card"
			fi
		fi
	'';

  purdue-auth = ''
  '';
in {
  environment.systemPackages = with pkgs; [
    macchanger
  ];

  systemd.services = builtins.listToAttrs (builtins.map (interface: {
    name = "macchanger-${interface}";
    value = {
  		enable = true;
  		description = "macchanger on ${interface}";
  		wants = [ "network-pre.target" ];
  		before = [ "network-pre.target" ];
  		bindsTo = [ "sys-subsystem-net-devices-${interface}.device" ];
  		after = [ "sys-subsystem-net-devices-${interface}.device" ];
  		wantedBy = [ "multi-user.target" ];
  		serviceConfig = {
  			Type = "oneshot";
  			ExecStart = "${change-mac} ${interface}";
  		};
    };
  }) config.networking.wireless.interfaces);

  age.secrets.wireless-environment-file.file = ./secrets/wireless-environment-file.age;
  # `network.wireless.interfaces` is expected to be set elsewhere
  networking.wireless = {
    enable = true;

    userControlled.enable = true;

    secretsFile = config.age.secrets.wireless-environment-file.path;

    networks = {
      "Magnetic_Moment".pskRaw = "ext:magnetic_moment_pass";
      "PACE245".pskRaw = "ext:pace245_pass";
      "Rovnyak WiFi".pskRaw = "ext:rovnyak_wifi_pass";
      "PAL3.0" = {
        auth = ''
          key_mgmt=WPA-EAP
          eap=PEAP
          phase2="auth=MSCHAPV2"
          identity="hrovnyak@purdue.edu"
          password=ext:purdue_pass
        '';
        priority = 2;
      };
      "eduroam" = {
        auth = ''
          key_mgmt=WPA-EAP
          eap=PEAP
          phase2="auth=MSCHAPV2"
          identity="hrovnyak@purdue.edu"
          password=ext:purdue_pass
        '';
        priority = 1;
      };
    };
  };
}
